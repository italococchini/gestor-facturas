<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class AuthController extends Controller
{
    public function signUp(Request $request){
        $validator = \Validator::make($request->all(),[
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string'
        ]);        
        if ($validator->fails()){
            return error($validator->errors()->all(), 400);
        }
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
        return success($user, 201);
    }

    public function login(Request $request){
        $validator = \Validator::make($request->all(),[
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        if ($validator->fails()){
            return error($validator->errors()->all(), 422);
        }
        $credentials = $request->only('email', 'password'); 
        if (!\Auth::attempt($credentials)) {
            return error(['message' => 'Unauthorized'], 401);
        }
        $user = $request->user();
        $hash = $user->createToken(rand());
        $token = $hash->plainTextToken;
        return success([
            'token' => $token
        ]);
    }

    public function logout(Request $request){
        $request->user()->token()->revoke();
        return success([
            'message' => 'Logout'
        ]);
    }

    public function me(Request $request){
        return success($request->user());
    }
}