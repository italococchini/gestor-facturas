<?php

if( !function_exists('success') ){
    function success( $data, $code = 200 ){        
        return response()->json($data, $code );
    }
}

if( !function_exists('error') ){
    function error( $data, $code = 400 ){
        return response()->json( $data, $code );
    }
}
